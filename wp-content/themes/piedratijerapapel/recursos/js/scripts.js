(function ($, root, undefined) {
	
	$(function () {
		
		'use strict';
		
		$(document).ready(function(){
 
			$.ajaxSetup({cache:false});
			
			$(document).on("click", ".post-link" , function() {
				var post_link = $(this).attr("href"),
					post_id = $(this).attr("rel"),
					post_parent_l = $(this).parents('section'), //for declare to the parent section the loading class
					whatid_parent = post_parent_l.data('letra'),
					li_block  = $(this).parents('li'); //know what categorie is
				
				
				$(post_parent_l).addClass('loadingeffect');
				$(li_block).addClass('open');
				
				$('#single-container-'+post_id).html('<div class="load sp" data-load="'+ whatid_parent +'"><span></span></div>');

				$('#single-container-'+post_id).load( post_link, function() {
					$('[rel='+ post_id +']').fadeOut();
					setTimeout(function(){ $(post_parent_l).removeClass('loadingeffect'); }, 1500);
				});
			 return false;
			});
			
			
			$(document).on("click", ".post-loop-link" , function() {
				var post_link = $(this).attr("href"),
					post_id = $(this).attr("rel"),
					post_parent_l = $(this).parents('.tagseleccionados'),
					whatid_parent_l = post_parent_l.data('letra'),
					li_block  = $(this).parents('li'); //know what categorie is;
                    console.log('parent id text:'+whatid_parent_l);
				    $(li_block).addClass('open');
				
                    $('#single-container-'+post_id).html('<div class="load sp" data-load="'+ whatid_parent_l +'"><span></span></div>');
                    $('#single-container-'+post_id).load( post_link, function() {
                        $('[rel='+ post_id +']').fadeOut();
                        setTimeout(function(){ $(post_parent_l).removeClass('loadingeffect'); }, 1500);
				});
			return false;
			});
			
			////tags
			$(document).on("click", ".tag-post" , function() {
				var tag_link = $(this).attr("href"),
					tag_text = $(this).text(),
					post_parent_l = $(this).parents('section'),
					parent_id = post_parent_l.attr('id');
				
				//$(post_parent_l).addClass('loadingeffect');
				/*$('#tag-container-'+parent_id).load( tag_link, function() {
					$(post_parent_l).addClass('view-tags');
					setTimeout(function(){ $(post_parent_l).removeClass('loadingeffect'); }, 1500);
				});*/
				$('#tagbloque').addClass('show');
				$('.tagseleccionados').attr('data-letra', tag_text);
				
				console.log('el titulo de tag es:' + tag_text);
				
				$('.tagseleccionados').html('<div class="load sp" data-load="'+ parent_id +'"></div>');
				
			
				$('.tagseleccionados').load( tag_link, function() {
					setTimeout(function(){ $(post_parent_l).removeClass('loadingeffect'); }, 1500);
				});
				
			return false;
			});
			
			///cerrar click parrafo
			$(document).on("click", ".article-detail p" , function() {
				var articleDetail = $(this).parents('main.article-detail'),
					blockParentArticle = $(this).parents('li');
				
				$(articleDetail).slideToggle('2500', function() {
					// Animation complete.
					$(blockParentArticle).find('.post-link').fadeIn();
					$(blockParentArticle).removeClass('open');
				  });
                
			     return false;
			});
		});
        
        $('[data-fancybox]').fancybox({
            // Options will go here
        });
        $(".post-back-list").click(function(){
            $('.post-list li').show();
        });
		 $('.busqueda.icn').click(function(){
            $('#busquedabloque').toggleClass('show');
            $('.busqueda.icn').toggleClass('ing');
        });
		$(document).on("click", ".cerrar" , function() {
			var cualcerrar = $(this).parents('.show'),
                idcualcerrar = cualcerrar.attr('id');
            $(cualcerrar).removeClass('show');
            
            if(idcualcerrar == 'busquedabloque') {
                $('.busqueda.icn').removeClass('ing');
            }
        });
	
		//////
		//
		//
		var arr = ['ta','tb','tc','td'];
        var idx = Math.floor(Math.random() * arr.length);
        document.body.classList.add(arr[idx]);
        
        
        $('header a, .mixer a').mouseenter(function(){
            var cual = $(this).data('action');
            $('[data-action="'+ cual +'"]').addClass('hover');
        });
        $('header a, .mixer a').mouseleave(function(){
            var cual = $(this).data('action');
            $('[data-action="'+ cual +'"]').removeClass('hover');
        });
        
        var interaction = false;
        
        $('header a, .mixer a').click(function(e){
            var cual = $(this).data('action');
            $('[data-action="'+ cual +'"]').toggleClass('activo');
            ///columna
            //$('header').addClass('cont-show');
            $('#'+ cual).toggleClass('show');
            $('#'+ cual).find('.call').toggleClass('activo');
            interaction = true;

            //document.body.classList('focus');
            /*la funcion para scroll to work area no funciona - mejor forzar temporalmente .focus*/
			$('body').addClass('focus');  //scroll body
            
            e.preventDefault();
        });
        
        $('.call').click(function(){
            //columna
            $(this).parent().toggleClass('show');
            //pestaña
            $(this).toggleClass('activo');
            //menú narrado, mixer
            var cual = $(this).find('h2').data('action');
            $('[data-action="'+ cual +'"]').toggleClass('activo');
            
            interaction = true;
            //document.body.classList('focus');
            $('body').addClass('focus');
        });
	

        ///----scroll to work area
        const totalH = document.body.offsetHeight;
        const area = document.querySelector('.content-section');
        const areaAltura = document.querySelector('.content-section').offsetTop;
        window.addEventListener("scroll", (event) => {
            var scroll = this.scrollY;
            if (scroll >= totalH/4){
                //area.scrollIntoView(true);
                //document.querySelector('.content-section').scrollIntoView({behavior: "smooth"});
                if (interaction == true) {
                    document.body.classList.add('focus');
                     window.scrollBy({ 
                      top: areaAltura, // could be negative value
                      left: 0, 
                      behavior: 'smooth' 
                    });
                }
               
            }
        });
        ///----scroll to top area
        const ptp = document.querySelector('.ptp a');
        const inicio = document.getElementById("ptp");
        ptp.addEventListener('click', function() {
            document.body.classList.remove('focus');
            inicio.scrollIntoView();
        });
        //drag
        /*------------------------------------*/
        var offset = [0,0];
        var mixer = document.querySelector('.mixer').parentElement;
        var isDown = false;

        mixer.addEventListener('mousedown', function(e) {
        isDown = true;
        offset = [
            mixer.offsetLeft - e.clientX,
            mixer.offsetTop - e.clientY
         ];
        }, true);

        document.addEventListener('mouseup', function() {
           isDown = false;
        }, true);

        document.addEventListener('mousemove', function(e) {
            event.preventDefault();
            if (isDown) {
                mixer.style.left = (e.clientX + offset[0]) + 'px';
                mixer.style.top  = (e.clientY + offset[1]) + 'px';
           }
        }, true);

		/*-----------------special for mac--------------------*/
        if(navigator.userAgent.indexOf('Mac') > 0) {
            
            function addStyle(styles) { 

                /* Create style document */ 
                var css = document.createElement('style'); 
                css.type = 'text/css'; 

                if (css.styleSheet)  
                    css.styleSheet.cssText = styles; 
                else  
                    css.appendChild(document.createTextNode(styles)); 

                /* Append style to the tag name */ 
                document.getElementsByTagName("head")[0].appendChild(css); 
            } 

            /* Set the style */ 
            var styles = '.tc header p a.activo {top: -.30em;}'; 
                styles += ' .ta header p a.activo {top: -.30em;}';
            //styles += ' body nav .mixer li:nth-child(4) {background:blue;}';

            /* Function call */ 
            window.onload = function() { addStyle(styles) }; 

        }
        /*-------------------------------------*/
        
	   if ( $( "#archivo" ).length ) {
            var sizes;
            if (sizes) {
                sizes = 200
            } else {
                sizes = [50, 50] // default sizes
            }



            var split = Split(['#archivo','#hilo', '#tienda', '#oficina'], {
                minSize: 50,

                elementStyle: (dimension, size, gutterSize) => ({
                    'flex-basis': `calc(${size}% - ${gutterSize}px)`,
                    'font-size': `${(size)/1.3}px`,
                }),
                gutterStyle: (dimension, gutterSize) => ({
                    'flex-basis':  `${gutterSize}px`,
                }),
                onDrag: function(sizes) {
                    localStorage.setItem('split-sizes', JSON.stringify(sizes));
                    console.log(split-sizes);
                    if ($('section').width() <= '200'){
                        $(this).addClass('none');
                    }

                    /*set font size value*/
                    /*var varSize = document.getElementById("hilo").getAttribute("style"),
                      valueSize = varSize,
                           part = valueSize.match(/\d/g);
                    document.getElementById("contenthilo").style.fontSize = ""+(part[2])/1.5+"vw";

                    console.log(part);*/
                },
            });
            split.setSizes([25, 25]);
	   }
		
});
	
})(jQuery, this);


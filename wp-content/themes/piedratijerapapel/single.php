<!-- ?php get_header(); ?-->

	<main class="article-detail" role="main">
	<!-- article -->
	<?php $post = get_post($_POST['id']);?>
    <article id="single-post-<?php the_ID(); ?>" <?php post_class(); ?>>
 
    <?php while (have_posts()) : the_post(); ?>
		
		<!-- post info -->
		<div class="post-info">
			<h2><?php the_title(); ?></h2>
			<?php if(get_field('author')){
				echo '<h3 class="author">Por: ' . get_field('author') . '</h3>';
			}?>
			<span class="date"><?php the_time('F j, Y'); ?></span>
		</div>
		<!-- /post info -->
		<?php the_post_thumbnail(); // Fullsize image for the single post ?>
		<?php the_content();?>
		
		<div class="tags">
		 	<?php the_tags( 'Tags: ', ', ', '<br />' ); ?> 
		</div>
		
    <?php endwhile;?> 
 
    </article>
		
	<!-- /article -->
		<!--
sanduche entradas relacionadas
		&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;
&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;<br>
		
		< ?php
			//for use in the loop, list 5 post titles related to first tag on current post
			$tags = wp_get_post_tags($post->ID);
			if ($tags) {
			echo 'Entradas relacionadas';
			$first_tag = $tags[0]->term_id;
			$args=array(
			'tag__in' => array($first_tag),
			'post__not_in' => array($post->ID),
			'posts_per_page'=>5,
			'caller_get_posts'=>1
			);
			$my_query = new WP_Query($args);
			if( $my_query->have_posts() ) {
			while ($my_query->have_posts()) : $my_query->the_post(); ?>
				<a style="color:red;" class="post-link" href="< ?php the_permalink() ?>" rel="post-< ?php the_ID(); ?>-b" title="ver < ?php the_title_attribute(); ?>">
					<h2>< ?php the_title(); ?></h2>
					< ?php the_post_thumbnail('thumbnail'); ?>
				</a>
				<div id="single-container-post-< ?php the_ID(); ?>-b"></div>
			< ?php
			endwhile;
			}
			wp_reset_query();
			}
			?>
		
		
		<br>
		&blk34;&blk34;&blk34;&blk34;&blk34;&blk34;&blk34;&blk34;&blk34;&blk34;&blk34;&blk34;&blk34;&blk34;&blk34;
		&blk34;&blk34;&blk34;&blk34;&blk34;&blk34;&blk34;&blk34;&blk34;&blk34;&blk34;&blk34;&blk34;&blk34;&blk34;
		&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;
&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;&blk14;
-->
	</main>


<!-- ?php get_sidebar(); ? >
< ?php get_footer(); ? -->


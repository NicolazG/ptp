<li data-post="post-<?php the_ID(); ?>">
	<a class="post-link" rel="post-<?php the_ID(); ?>" href="<?php the_permalink(); ?>">
        <!-- post info -->
		<div class="post-info">
			<h2><?php the_title(); ?></h2>
			<?php if(get_field('author')){
				echo '<h3 class="author">Por: ' . get_field('author') . '</h3>';
			}?>
			<span class="date"><?php the_time('F j, Y'); ?></span>
		</div>
		<!-- /post info -->
        <!--<?php if( has_post_thumbnail() ){?><?php the_post_thumbnail ('thumbnail'); ?><?php }?>-->
	</a>
	

	
	<?php 
		$images = get_field('multiple_image_field');
		$i = 0;
		$len = count($images);
		$galTipo = get_field('tipo');
		if( $images ): ?>
			<ul class="galeria tipo-<?php echo $galTipo; ?> grid">
				<?php foreach( $images as $image ): ?>
				<?php if ($i == 0) { ?>
					<li class="large-<?php if ($galTipo == 'a') { echo 'col-8'; } else if ($galTipo == 'b') {echo 'col-12'; } else {echo 'col-12';}?>">
						<a data-fancybox="gallery<?php the_ID(); ?>" href="<?php echo esc_url($image['url']); ?>">
							<img style="width:100%" src="<?php echo esc_url($image['sizes']['thumbnail']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
						</a>
						<p><?php echo esc_html($image['caption']); ?></p>
					</li>
				<?php } else if ($i == 1) { ?>
					<?php if ($galTipo == a || $galTipo == c) { ?>
					<li class="large-<?php if ($galTipo == 'a') { echo 'col-4'; } else {echo 'col-12 c2';}?>">
						<div>
							<a data-fancybox="gallery<?php the_ID(); ?>" href="<?php echo esc_url($image['url']); ?>">
								<img src="<?php echo esc_url($image['sizes']['thumbnail']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
							</a>
							<p><?php echo esc_html($image['caption']); ?></p>
						</div>
					<?php }  else {?>
						<li class="col-8">
							<div>
								<a data-fancybox="gallery<?php the_ID(); ?>" href="<?php echo esc_url($image['url']); ?>">
									<img src="<?php echo esc_url($image['sizes']['thumbnail']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
								</a>
								<p><?php echo esc_html($image['caption']); ?></p>
							</div>
						</li>
					<?php } ?>
                <?php } else if ($i == 2) { ?>
                    <?php if ($galTipo == a || $galTipo == c) { ?>
						<div>
							<a data-fancybox="gallery<?php the_ID(); ?>" href="<?php echo esc_url($image['url']); ?>">
								<img src="<?php echo esc_url($image['sizes']['thumbnail']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
							</a>
							<p><?php echo esc_html($image['caption']); ?></p>
						</div>
					</li>
					<?php }  else {?>
						<li class="col-4">
							<div>
								<a data-fancybox="gallery<?php the_ID(); ?>" href="<?php echo esc_url($image['url']); ?>">
									<img src="<?php echo esc_url($image['sizes']['thumbnail']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
								</a>
								<p><?php echo esc_html($image['caption']); ?></p>
							</div>
						
					<?php } ?>
                <?php } else if ($i == 3) { ?>
                    <?php if ($galTipo == a || $galTipo == c) { ?>
						<li class="col-12">
							<a data-fancybox="gallery<?php the_ID(); ?>" href="<?php echo esc_url($image['url']); ?>">
								<img src="<?php echo esc_url($image['sizes']['thumbnail']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
							</a>
							<p><?php echo esc_html($image['caption']); ?></p>
						</li>
					<?php }  else {?>
						
							<div>
								<a data-fancybox="gallery<?php the_ID(); ?>" href="<?php echo esc_url($image['url']); ?>">
									<img src="<?php echo esc_url($image['sizes']['thumbnail']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
								</a>
								<p><?php echo esc_html($image['caption']); ?></p>
							</div>
						</li>
					<?php } ?>
				<?php } else if ($i == $len - 1) /*el ultimo*/ { ?>
					<li class="este es ultimo">
						<a data-fancybox="gallery<?php the_ID(); ?>" href="<?php echo esc_url($image['url']); ?>">
							<img src="<?php echo esc_url($image['sizes']['thumbnail']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
						</a>
						<p><?php echo esc_html($image['caption']); ?></p>
					</li>
				<?php } else  /*el resto*/ { ?>
						<li>
							<a data-fancybox="gallery<?php the_ID(); ?>" href="<?php echo esc_url($image['url']); ?>">
								<img src="<?php echo esc_url($image['sizes']['thumbnail']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
							</a>
							<p><?php echo esc_html($image['caption']); ?></p>
						</li>
		 		<?php }$i++;?>
					
				<?php endforeach; ?>
			</ul>
		<?php endif; ?>
	
	
	<p><?php the_excerpt(); ?></p>
	
    <div id="single-container-post-<?php the_ID(); ?>"></div>
		
	<!--<?php edit_post_link(); ?>-->
	<span class="bottom-post-arrow"></span>
</li>
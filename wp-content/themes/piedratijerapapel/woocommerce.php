<?php get_header(); ?>
<style type="text/css">
	header, nav {display:none;}
</style>
	<main class="content-section" role="woocommerce">
		<!-- section -->
		<section class="show">
		<div class="content-scroll">
			<h1><?php the_title(); ?></h1>
		<?php woocommerce_content(); ?>
			</div>
		

		</section>
		<!-- /section -->
	</main>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
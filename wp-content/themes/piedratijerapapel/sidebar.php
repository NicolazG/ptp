<!-- sidebar -->

<div id="busquedabloque">
    <div class="busquedabarra">
        <?php get_template_part('searchform'); ?>
        <span class="cerrar"><span>✕</span></span>
    </div>
    <div class="busquedacaja">
        <div class="resultados"></div>
    </div>
</div>
<div id="tagbloque">
    <div class="tagcaja">
        <div class="tagseleccionados" data-letra=""></div>
    </div>
</div>	


<aside class="sidebar" role="complementary">

	<div class="sidebar-widget">
		<?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('widget-area-1')) ?>
	</div>

	<div class="sidebar-widget">
		<?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('widget-area-2')) ?>
	</div>

</aside>
<!-- /sidebar -->

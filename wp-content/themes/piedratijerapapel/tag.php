<div class="tagbarra">
	<h1><?php _e( 'Tag Archive: ', 'html5blank' ); echo single_tag_title('', false); ?></h1>
	<span class="cerrar"><span>✕</span></span>
</div> 	
<main role="tag-archive">
	<ul class="post-list">
		<?php get_template_part('loop'); ?>
	</ul>
	<?php get_template_part('pagination'); ?>		
</main>
<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
        
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>
        <meta name="description" content="<?php bloginfo('description'); ?>">
        
        
		<link href="//www.google-analytics.com" rel="dns-prefetch">
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/recursos/img/icons/ptp.ico">
        <link rel="apple-touch-icon-precomposed" type="image/png" href="<?php echo get_template_directory_uri(); ?>/recursos/img/icons/ptp.png">

		<?php wp_head(); ?>
		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script>

	</head>
	<body <?php body_class(); ?>>
	<!-- header -->
		<header id="ptp">
				<h3>Hola*</h3>
				<p>En este ediﬁcio hay cuatro timbres; uno es el <a class="sp" href="#" data-action="archivo">archivo</a> sobre la historia del diseño gráﬁco en colombia. En el segundo hay un <a class="sp" href="#" data-action="hilo">hilo</a> de conversación sobre el archivo y otros temas. En el tercero, la <a  class="sp"href="#" data-action="tienda">tienda</a>, un espacio comercial donde se encuentran tipografía y publicaciones hechas en Colombia.Y en el cuarto, la <a class="sp" href="#" data-action="oficina">oﬁcina</a> de diseño y edición de Juan Pablo Fajardo.</p>	
				<div class="nota">* El menú volador es un mezclador con el cual se pueden activar las entradas que tenga ganas de ver.</div>
    	</header>
	<!-- /header -->
		
    <nav draggable="true">        
        <ul class="mixer sp">
            <li><h1 class="ptp"><a href="#ptp">PiedraTijeraPapel</a></h1></li>
            <li><a data-action="archivo" href="#"><span>A</span></a></li>
            <li><a data-action="hilo" href="#"><span>H</span></a></li>
            <li><a data-action="tienda" href="#"><span>T</span></a></li>
            <li><a data-action="oficina" href="#"><span>O</span></a></li>
            <li><a class="busqueda icn"><span>Búsqueda</span></a></li>
        </ul>
    </nav>
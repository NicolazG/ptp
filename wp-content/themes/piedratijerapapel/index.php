<?php get_header(); ?>


<div class="content-section">
	
	<section id="archivo">
        <div class="call"></div>
        <div class="content-scroll">

			<!--
			<h3>todos</h3>
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<a class="post-link" rel="<?php the_ID(); ?>" href="<?php the_permalink(); ?>">
					<?php the_title(); ?> 
				</a>
    		<?php endwhile; endif; ?>
			-->

		</div>
	</section>
	
	
	
	<section id="hilo" data-letra="h">
        <div class="call"><h2 class="sp" data-action="hilo">hilo</h2></div>
        <div class="content-scroll">
			<?php echo do_shortcode('[ajax_load_more container_type="ul" css_classes="post-list" post_type="post" category="hilo" scroll_container="#hilo"  button_label="Posts anteriores" button_done_label="---no hay más contenido que cargar ---"]');?>
			<div id="tag-container-hilo"></div>
        </div>
    </section>
    
    <section id="tienda" data-letra="tienda">
		<div class="call"><h2 class="sp"data-action="tienda">tienda</h2></div>
        <div class="content-scroll">
			<ul class="post-list">
			<?php $args = array(
                    'post_type' => 'product',
                    'posts_per_page' => 12
                    );
                $loop = new WP_Query( $args );
                if ( $loop->have_posts() ) {
                    while ( $loop->have_posts() ) : $loop->the_post();
                        wc_get_template_part( 'content', 'product' );
                    endwhile;
                } else {
                    echo __( 'No products found' );
                }
                wp_reset_postdata();
            ?>
			</ul>
			
			<!--?php echo do_shortcode('[ajax_load_more container_type="ul" css_classes="post-list" post_type="post" category="tienda"  button_label="Posts anteriores" button_done_label="---no hay más contenido que cargar ---"]');?>
			<div id="tag-container-tienda"></div-->
			
        </div>
    </section>
	
    <section id="oficina" data-letra="o">
		<div class="call"><h2 class="sp" data-action="oficina">oﬁcina</h2></div>
        <div class="content-scroll">

            <!--?php echo do_shortcode('[ajax_load_more container_type="div" post_type="post" category="oficina"]');?-->
            
            
			<?php $catquery = new WP_Query( 'cat=4&posts_per_page=5' ); ?>
			<ul class="post-list">
				<?php while($catquery->have_posts()) : $catquery->the_post(); ?>
				<li data-post="post-<?php the_ID(); ?>">
					<a class="post-link" rel="post-<?php the_ID(); ?>" href="<?php the_permalink(); ?>">
						<!-- post info -->
						<div class="post-info">
							<h2><?php the_title(); ?></h2>
							<?php if(get_field('author')){
								echo '<h3 class="author">Por: ' . get_field('author') . '</h3>';
							}?>
							<span class="date"><?php the_time('F j, Y'); ?></span>
						</div>
						<!-- /post info -->
						<?php the_post_thumbnail('thumbnail'); ?>
						<p><?php the_excerpt(); ?></p>
					</a>
					<div id="single-container-post-<?php the_ID(); ?>"></div>
				</li>
			<?php endwhile; wp_reset_postdata();?>
			</ul>

        </div>
    </section>

</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
<?php if (have_posts()): while (have_posts()) : the_post(); ?>

	<!-- article -->
	<li id="post-loop-<?php the_ID(); ?>" <?php post_class(); ?> data-post="post-<?php the_ID(); ?>">
		<a class="post-loop-link" rel="post-loop-<?php the_ID(); ?>" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
			
			<!-- post info -->
			<div class="post-info">
				<h2><?php the_title(); ?></h2>
				<?php if(get_field('author')){
					echo '<h3 class="author">Por:' . get_field('author') . '</h3>';
				}?>
				<!--<h3 class="author"><?php _e( 'Por:', 'html5blank' ); ?> <?php the_author(); ?></h3>-->
				<span class="date"><?php the_time('F j, Y'); ?></span>
			</div>
			<!-- /post info -->
			
			<?php $images = get_field('multiple_image_field');
            if( $images ): ?>
                <ul>
                    <?php foreach( $images as $image ): ?>
                        <li>
                            <a data-fancybox="loop-gallery<?php the_ID(); ?>" href="<?php echo esc_url($image['url']); ?>">
                                 <img src="<?php echo esc_url($image['sizes']['thumbnail']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                            </a>
                            <p><?php echo esc_html($image['caption']); ?></p>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
			
			<!-- post details -->
			<?php html5wp_excerpt('html5wp_index'); // Build your custom callback length in functions.php ?>
			<!-- /post details -->

		</a>
		
		<div id="single-container-post-loop-<?php the_ID(); ?>"></div>
		
		<?php edit_post_link(); ?>
		<span class="bottom-post-arrow"></span>
	</li>
	<!-- /article -->

<?php endwhile; ?>

<?php else: ?>

	<!-- article -->
	<li>
		<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
	</li>
	<!-- /article -->

<?php endif; ?>

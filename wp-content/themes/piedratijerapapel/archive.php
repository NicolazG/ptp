<main role="archive">
		<h1><?php _e( 'Archives', 'html5blank' ); ?></h1>

		<ul class="post-list">
			<?php get_template_part('loop'); ?>
		</ul>

		<?php get_template_part('pagination'); ?>
</main>